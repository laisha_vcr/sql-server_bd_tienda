<?php
include("conexion.php");
?>

<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <title>Crédito</title>
    <style>
        .content {
            margin-top: 30px;
        }
    </style>

</head>

<body>
<?php
    echo include("navbar.php");
    ?>

    <div class="container">
        <div class="content">
            <h2 align="center">Creditos</h2>

            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <tr>
                        <td> </td>
                        <td>Cargo</td>
                        <td>Abono</td>
                        <td>Movimiento</td>
                        <td>Venta</td>
                        <td>Nombre</td>
   
                    </tr>
                    <?php

                    $sql = "SELECT * FROM movimiento, usuario, venta WHERE  movimiento.fk_ventaID = venta.vtID AND movimiento.fk_usuarioID = usuario.usID";
                    $creditos = eSQL($sql);

                    if (!$creditos) {
                        echo '<tr><td colspan="8">No hay datos.</td></tr>';
                    } else {

                        foreach ($creditos as $elemento) {
                            echo '
						<tr>
							<td>  </td>

                            <td>' . $elemento['cargo'] . '</td>
                            <td>' . $elemento['abono'] . '</td>
							<td>' . $elemento['monto_mov'] . '</td>
                            <td>' . $elemento['montoTotal'] . '</td>
                            <td>' . $elemento['nombre_usu'] . '</td>
							<td>';

                        }
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>

</body>

</html>