<?php
include("conexion.php");
$con = conectar();
?>

<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <title>MyStoreLand</title>
    <style>
        .content {
            margin-top: 30px;
        }
    </style>

</head>

<body>
    <?php
    echo include("navbar.php");
    ?>
    <div class="container">
        <div class="content">
            <h2 align="center">Inventario</h2>

            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <tr>
                        <td> </td>
                        <td>Nombre</td>
                        <td>Stock</td>
                        <td>Descripción</td>
                        <td>Precio</td>
                        <td>Marca</td>
                        <td>Departamento</td>
                        <td>Proveedor</td>

                    </tr>
                    <?php
                    $sql = "SELECT * FROM producto, usuario, departamento WHERE producto.fk_usProveedor = usuario.usID AND producto.fk_depID = departamento.depID ORDER BY usID ASC";
                    $inventario = eSQL($sql);

                    if (!$inventario) {
                        echo '<tr><td colspan="8">No hay datos.</td></tr>';
                    } else {
                        foreach ($inventario as $elemento) {
                            echo '
                            <tr>
                                <td>  </td>
    
                                <td>' . $elemento['pdNombre'] . '</td>
                                <td>' . $elemento['stock'] . '</td>
                                <td>' . $elemento['descripcion'] . '</td>
                                <td>' . $elemento['precio'] . '</td>
                                <td>' . $elemento['marca'] . '</td>
                                <td>' . $elemento['depNombre'] . '</td>
                                <td>' . $elemento['nombre_usu'] . '</td>
                                <td>';
    
                                echo '
                                </td>
                                <td>
    
                                    <a href="index.php?sup=delete&nik=' . $elemento['pdID'] . '" title="Eliminar" onclick="return confirm(\'Esta seguro de borrar los datos ' . $elemento['pdNombre'] . '?\')" class="btn btn-danger btn-m">Eliminar</a>
                                    
                                    </td>
                            </tr>
                            ';
                        }
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>

</body>

</html>